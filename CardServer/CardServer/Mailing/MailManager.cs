﻿using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using CardServer.Console;

namespace CardServer.Mailing
{
    internal class MailManager
    {
        public static void Send(string receiver, string subject, string body)
        {
            SendEmailAsync(receiver, subject, body).GetAwaiter();
        }

        private static async Task SendEmailAsync(string receiver, string subject, string body)
        {
            var from = new MailAddress("callofmyth@gmail.com", "CallOfMyth Team");
            var to = new MailAddress("ysarchuk@gmail.com");
            var m = new MailMessage(from, to) {Subject = subject, Body = body};
            var smtp = new SmtpClient("smtp.gmail.com", 587)
            {
                Credentials = new NetworkCredential(Constants.GMAIL_USER, Constants.GMAIL_PASSWORD),
                EnableSsl = true
            };
            await smtp.SendMailAsync(m);
            DeveloperConsole.Log($"Email to {receiver} successfully sent");
        } // Отправляет письмо на почту
    }
}