﻿namespace CardServer.Net
{
    // Типы пакетов которые отправляются сервером(с префиксом S) и типы пакетов которые отправляются клиентом (с префиксом C)
   public enum ServerPackets
{
    SWelcome = 1,
    SAlertMsg,
    SMatchStart,
}
public enum ClientPackets
{
    CRegisterAccount = 1,
    CLoginAccount,
    CSearchForRoom,
    CRecieveOK,
}
}
