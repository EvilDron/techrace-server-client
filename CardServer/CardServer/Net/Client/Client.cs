﻿using System;
using System.Net.Sockets;
using CardServer.Console;
using CardServer.Game.Room;
using CardServer.Net.Server;

namespace CardServer.Net.Client
{
    internal class Client
    {
        public int Index { get; set; } // Локальный индекс клиента
        public int RoomIndex { get; set; } // Индекс в комнате в которой он находиться( -1 = не находится в комнате)
        public string Ip { get; set; } // IP пользователя
        public TcpClient Socket1 { get; set; } // Сокет данного клиента
        public NetworkStream MyStream; // Поток общения с клиентом

        public bool LoggedIn { get; private set; } // Авторизован ли игрок
        public int GetRating { get; private set; } // Рейтинг игрока
        public string GetName { get; private set; } // Имя игрока
        

        private byte[] _readBuff;

        public void Start()
        {
            Socket1.SendBufferSize = 4096;
            Socket1.ReceiveBufferSize = 4096;
            MyStream = Socket1.GetStream();
            _readBuff = new byte[4096];
            MyStream.BeginRead(_readBuff, 0, Socket1.ReceiveBufferSize, OnReceiveData, null);

        }  // Инициализация всего необходимого для общения с клиентом
        public void LogIn(string name) 
        {
            GetName = name;
            GetRating = Database.Database.GetRating(GetName);
            ServerTCP.SendAlertTo(Index, "Your rating " + GetRating.ToString());
            LoggedIn = true;
        }// Вызывается при удачной авторизации клиента
        public void RoomFree()
        {
            if (RoomIndex != -1)
                RoomManager.FreeRoom(Index);
        } // Освобождение комнаты

        private void OnReceiveData(IAsyncResult ar)
        {
            try
            {
                var readbytes = MyStream.EndRead(ar);
                if (readbytes <= 0)
                {
                    CloseSocket();
                    RoomFree();
                    return;
                }
                byte[] newBytes = new byte[readbytes];
                Buffer.BlockCopy(_readBuff, 0, newBytes, 0, readbytes);
                ServerHandleData.HandleData(Index, newBytes);
                MyStream.BeginRead(_readBuff, 0, Socket1.ReceiveBufferSize, OnReceiveData, null);

            }
            catch (Exception ex)
            {
                CloseSocket(ex.Message);
                return;
            }
        } // Обрабатываем получение данных от клиента

        private void CloseSocket()
        {
            DeveloperConsole.Log("Connection " + Ip + " has been closed.");
            ServerTCP.OnlineCount--;
            Socket1.Close();
            Socket1 = null;
        } // Закрывает сокет

        private void CloseSocket(string s)
        {

            if (Socket1 == null) return;
            DeveloperConsole.Log("Connection " + Ip + " has been closed." + s);
            ServerTCP.OnlineCount--;
            Socket1.Close();
            Socket1 = null;
        } // Перегрузка с выводом информации

    }
}
