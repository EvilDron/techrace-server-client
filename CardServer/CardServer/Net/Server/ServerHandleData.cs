﻿using System;
using System.Collections.Generic;
using CardServer.Console;
using CardServer.Game;
using CardServer.Game.Room;

namespace CardServer.Net.Server
{
    public class ServerHandleData
    {
        private delegate void Packet(long index, byte[] data); // Сигнатура всех методов обрабатывающих пакеты
        private static readonly Dictionary<long, Packet> Packets = new Dictionary<long, Packet>(); //Словарь делегатов, ключ - тип пакета, значение - делегат который будет его обрабатывать

        public static void InitPackets()//Инициализируем словарь
        {

            DeveloperConsole.Log("Initialize server...", ConsoleColor.Green);
            Packets.Add((long)ClientPackets.CRegisterAccount, Packet_CRegisterAccount);
            Packets.Add((long)ClientPackets.CLoginAccount, Packet_CLoginAccount);
            Packets.Add((long)ClientPackets.CSearchForRoom, Packet_CSearchForRoom);
            Packets.Add((long)ClientPackets.CSpawnCube, Packet_CSpawnCube);
            Packets.Add((long)ClientPackets.CDestroyCube, Packet_CDestroyCube);
            Packets.Add((long)ClientPackets.CReceiveOK, Packet_CReceiveOK);

        }

        #region Распознавание полученных от клиента пакетов

        public static void HandleData(long index, byte[] data)
        {

            var buffer = new ByteBuffer();
            buffer.WriteBytes(data);
            var packetnum = buffer.ReadInteger();
            buffer = null;

            if (packetnum == 0)
                return;

            if (Packets.TryGetValue(packetnum, out var packet))
            {
                packet.Invoke(index, data);
            }

        } // Определяет тип пакета и вызывает соответсвующий ему делегат

        #endregion

        #region PocketProcessing
        private static void Packet_CRegisterAccount(long index, byte[] data)
        {
            var buffer = new ByteBuffer(); // Создаём пустой экземпляр буфера
            buffer.WriteBytes(data); // Заполняем его поступившими байтами        
            var packetNum = buffer.ReadLong(); // Первые sizeof(long) байт всегда указывают на тип пакета
            var username = buffer.ReadString(); // Читаем имя пользователя
            var password = buffer.ReadString(); // Читаем пароль пользователя
            var email = buffer.ReadString(); // Читаем почту пользователя
            if (Database.Database.AddAccount(username, password, email)) // Если регистрация прошла успешно выводим данныев консоль(Было добавлено для отладки)
            {
                DeveloperConsole.Log(
                    $"New Registration:\n" +
                    $"               Login: {username}\n" +
                    $"               Password Hash: {password}\n" +
                    $"               Email: {email}");

            }
        } // Обрабатывает пакет типа регистрации(Далее по аналогии)
        private static void Packet_CLoginAccount(long index, byte[] data)
        {
            var buffer = new ByteBuffer();
            buffer.WriteBytes(data);
            var packetNum = buffer.ReadLong();
            var username = buffer.ReadString();
            var password = buffer.ReadString();

            if (!Database.Database.AccountExist(index, username))
            {
                return;
            }
            if (!Database.Database.PasswordOk(index, username, password))
            {
                return;
            }
            DeveloperConsole.Log($"User {username} has been successfully logged in!");
            ServerTCP.SendAlertTo(index, "You logged in!");
            ServerTCP.Clients[index].LogIn(username);

        }
        private static void Packet_CSearchForRoom(long index, byte[] data)
        {


            if (ServerTCP.Clients[index].LoggedIn)
            {
                DeveloperConsole.Log($"User {ServerTCP.Clients[index].GetName} is searching for opponent!");
                ServerTCP.SendAlertTo(index, "You are in search!");
                RoomManager.PutInSearch(index);
            }
            else
            {
                ServerTCP.SendAlertTo(index, "You are not logged in!");
            }

        }
        private static void Packet_CSpawnCube(long index, byte[] data)
        {
            if (!PlayerManager.IsInRoom(index)) return;
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteBytes(data);
            var packetNum = buffer.ReadLong();
            var x = buffer.ReadInteger();
            var y = buffer.ReadInteger();
            RoomManager.TrySpawnCube(index, x, y);

        }
        private static void Packet_CDestroyCube(long index, byte[] data)
        {
            if (!PlayerManager.IsInRoom(index)) return;
            var buffer = new ByteBuffer();
            buffer.WriteBytes(data);
            var packetNum = buffer.ReadLong();
            var x = buffer.ReadInteger();
            var y = buffer.ReadInteger();
            RoomManager.TryDeleteCube(index, x, y);

        }

        private static void Packet_CReceiveOK(long index, byte[] data)
        {

            var buffer = new ByteBuffer();
            buffer.WriteBytes(data);
            buffer.ReadLong(); 
            DeveloperConsole.Log($"{buffer.ReadString()} - from client with id = "+index.ToString(), ConsoleColor.Green);

        }
        #endregion
    }

}
