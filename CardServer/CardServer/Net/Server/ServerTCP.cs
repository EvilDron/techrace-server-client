﻿using System;
using System.Net;
using System.Net.Sockets;
using CardServer.Console;
using CardServer.Game;

namespace CardServer.Net.Server
{
    internal class ServerTCP
    {
        
        public static Client.Client[] Clients = new Client.Client[Constants.MAX_PLAYER]; // Создаём список для новых подключений
        public static int OnlineCount = 0; // Кол-во подключений


        private static TcpListener _serverSocket; // Экземпляр класса, который и будет прослушивать порт

        public static void InitNetwork()
        {
            _serverSocket = new TcpListener(IPAddress.Any, 5555); // Задаём параметры прослушки
            _serverSocket.Start(); // Начинаем прослушку
            _serverSocket.BeginAcceptTcpClient(OnClientConnect, null); // Включаем процесс принятия новых подключений 

            DeveloperConsole.Log("Server has been started!", ConsoleColor.Green);
        }

        public static void SendDataTo(long index, byte[] data)
        {
            var buffer = new ByteBuffer();
            buffer.WriteLong((data.GetUpperBound(0) - data.GetLowerBound(0)) + 1); // Пишем длину данных, чтобы с другой стороны было удобнее читать
            buffer.WriteBytes(data); // Записываем данные в буфер

            Clients[index].MyStream.BeginWrite(buffer.ToArray(), 0, buffer.ToArray().Length, null, null); // Начинаем отправку сообщения
        } // Отправляет байты информации клиенту с указанным id 
    
        private static void OnClientConnect(IAsyncResult ar)
        {

            var client = _serverSocket.EndAcceptTcpClient(ar); // Принимаем входящее подключение 
            client.NoDelay = false;
            _serverSocket.BeginAcceptTcpClient(OnClientConnect, null); // Открываем возомжность подключениям

            for (var i = 0; i < Constants.MAX_PLAYER; i++)
            {
                if (Clients[i].Socket1 != null) continue; // Если в данном экземпляре класса Client подключение не равно null, то он уже используется другим клиентом
                Clients[i].Socket1 = client; // Заполняем сокет нашим новым подключением
                Clients[i].Index = i; // Оставляем в клиенте данные, какой он по счёту
                Clients[i].Ip = client.Client.RemoteEndPoint.ToString(); // Записываем его IP
                Clients[i].Start(); // Инициализируем его начальное состояние
                DeveloperConsole.Log("Initialize connection with " + Clients[i].Ip);
                OnlineCount++; // Увеличиваем счётчик онлайна

                return;
            } // Ищем пустой слот для клиента

        } // Обрабатываем новое подключение

        
        #region PacketsToClient


        public static void SendAlertTo(long index, string msg)
        {
            var buffer = new ByteBuffer();
            buffer.WriteLong((long)ServerPackets.SAlertMsg); // Первыми байтами всегда должны быть байты идентифицирующие пакет
            buffer.WriteString(msg); // Записываем строку с сообщением
            SendDataTo(index, buffer.ToArray()); // Отправляем пакет

        } // Отправляет на клиент сообщения типа SAlertMsg (Далее по аналогии)





        public static void SendGameStartTo(long index, long opponent)
        {
            var buffer = new ByteBuffer();
            buffer.WriteLong((long)ServerPackets.SMatchStart);
            buffer.WriteString(PlayerManager.GetName(opponent));

            SendDataTo(index, buffer.ToArray());

        }


        #endregion

    }

}
