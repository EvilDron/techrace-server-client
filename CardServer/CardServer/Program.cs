﻿using System;
using System.Threading;
using CardServer.Console;
using CardServer.Database;
using CardServer.Net.Client;
using CardServer.Net.Server;

namespace CardServer
{
    internal class Program
    {

        private static Thread _threadConsole;//Создаём отдельный поток для консоли, чтобы сервер работал, а не только ожидал ввод команды

        private static void Main(string[] args)
        {
            _threadConsole = new Thread(new ThreadStart(ConsoleThread)); // Создаём поток и определяем что в нём будет происходить
            _threadConsole.Start(); // Запускаем поток
            SetupServer(); // Запускаем инициализацию сервера
        }

        private static void SetupServer()
        {
            MySQL.MySQLInit();  // Инициализируем БД
            DeveloperConsole.InitConsole(); // Инициализируем консоль
            ServerHandleData.InitPackets(); // Инициализируем словарь пакетов(в классе более подробно расписано что это)
            ServerTCP.InitNetwork(); // Инициализируем систему взаимодейсия Клиент-Сервер(в классе более подробно расписано что это)

            for (var i = 0; i < Constants.MAX_PLAYER; i++) // Заполняем список клиентов пустышками
            {
                ServerTCP.Clients[i] = new Client();
            }

        }

        private static void ConsoleThread()
        {
            while (true)
            {
                DeveloperConsole.ParseInput(System.Console.ReadLine());
            }
        } // Запускаем бесконечное считывание и обработку команд в отдельном потоке


    }

}
