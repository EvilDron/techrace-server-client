﻿using System;
using ADODB;
using CardServer.Console;

namespace CardServer.Database
{
    internal class MySQL
    {
        public static Recordset DB_RS;
        public static Connection DB_CONN;

        public static void MySQLInit()
        {
            try
            {
                DB_RS = new Recordset();
                DB_CONN = new Connection
                {
                    ConnectionString = $"Driver={{MySQL ODBC 3.51 Driver}};" +
                                       $"Server={Constants.DATABASE_IP};" +
                                       $"Port={Constants.DATABASE_PORT};" +
                                       $"Database={Constants.DATABASE_NAME};" +
                                       $"User={Constants.DATABASE_USER};" +
                                       $"Password={Constants.DATABASE_PASSWORD};Option=3;",
                    CursorLocation = CursorLocationEnum.adUseServer
                };

                DB_CONN.Open();
                DeveloperConsole.Log("Successfully connected to MySQL database!", ConsoleColor.Green);
            }
            catch (Exception ex)
            {
                DeveloperConsole.Log(ex.Message, ConsoleColor.Red);
            }
        } // Пытаемся подключиться к БД (Подробнее всё написано в документации ADODB)

    }
}
