﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using ADODB;
using CardServer.Console;
using CardServer.Mailing;
using CardServer.Net.Server;

namespace CardServer.Database
{
    internal class Database
    {
        private static readonly MD5 Md5Hash = MD5.Create();
        private static string GetMd5Hash(HashAlgorithm md5Hash, string input)
        {
            var data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            var sBuilder = new StringBuilder();

            foreach (var t in data)
            {
                sBuilder.Append(t.ToString("x2"));
            }

            return sBuilder.ToString();
        }

        public static bool AddAccount(string username, string password, string email)
        {

            var db = MySQL.DB_RS;
            {
                db.Open("SELECT * FROM accounts WHERE Username='" + username + "'", MySQL.DB_CONN, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockOptimistic);

                if (!db.EOF)
                {
                    db.Close();
                    return false;
                }
                db.Close();

                db.Open("SELECT * FROM accounts WHERE 0=1", MySQL.DB_CONN, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockOptimistic);
                db.AddNew();
                db.Fields["Username"].Value = username;
                db.Fields["Password"].Value = GetMd5Hash(Md5Hash, password);
                db.Fields["Email"].Value = email;
                db.Fields["Rating"].Value = 12;
                var random = new Random();
                db.Fields["Hash"].Value = GetMd5Hash(Md5Hash, random.Next(0, 10000).ToString());
                db.Update();
                db.Close();
                SendVerification(username);
                return true;
            }
        } // Добавляет аккаунт с указанными данными
        public static int GetRating(string username)
        {
            var db = MySQL.DB_RS;
            {

                db.Open("SELECT * FROM accounts WHERE Username='" + username + "'", MySQL.DB_CONN, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockOptimistic);

                if (db.EOF)
                {
                    db.Close();
                    return -1;
                }
                else
                {
                    var result = (int)db.Fields["Rating"].Value;
                    db.Close();
                    return result;

                }

            }
        } // Получает рейтинг из БД
        public static bool AccountExist(long index, string username)
        {
            var db = MySQL.DB_RS;
            {

                db.Open("SELECT * FROM accounts WHERE Username='" + username + "'", MySQL.DB_CONN, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockOptimistic);

                if (db.EOF)
                {
                    ServerTCP.SendAlertTo(index, "User with this login doesn't exist!");
                    db.Close();
                    return false;
                }
                else
                {
                    db.Close();
                    return true;
                }

            }
        } // Проверяет существует ли аккаунт с данным username.
        public static bool PasswordOk(long index, string username, string password)
        {
            var db = MySQL.DB_RS;
            {

                db.Open("SELECT '" + username + "' FROM accounts WHERE Password='" + GetMd5Hash(Md5Hash, password) + "'", MySQL.DB_CONN, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockOptimistic);

                if (db.EOF)
                {
                    ServerTCP.SendAlertTo(index, "Incorrect password");

                    db.Close();
                    return false;
                }
                else
                {
                    db.Close();
                    return true;
                }

            }
        } // Проверяет пароль на корректность
        public static bool DeleteAccount(string username)
        {
            var db = MySQL.DB_RS;
            {
                db.Open("SELECT * FROM accounts WHERE Username='" + username + "'", MySQL.DB_CONN, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockOptimistic);


                if (db.EOF)
                {
                    DeveloperConsole.Log("No such user", ConsoleColor.Red);

                    db.Close();
                    return false;
                }
                else
                {
                    DeveloperConsole.Log("User successfully deleted", ConsoleColor.Green);
                    db.Delete();
                    db.Update();
                    db.Close();
                    return true;
                }

            }
        }  // Удаляет аккаунт из БД
        public static bool SendVerification(string username)
        {
            var db = MySQL.DB_RS;
            {
                db.Open("SELECT Email,Hash FROM accounts WHERE (Username='" + username + "') AND (Verificated=0)", MySQL.DB_CONN, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockOptimistic);


                if (db.EOF)
                {

                    DeveloperConsole.Log("No such user", ConsoleColor.Red);

                    db.Close();
                    return false;
                }
                else
                {
                    var info = db.GetString(StringFormatEnum.adClipString, 1, " ").Split(' ');

                    MailManager.Send(info[0], "Account Verification", $"Your verification code: {info[1]}");
                    db.Close();
                    return true;
                }

            }
        } // Получает код потдтверждения из БД и отправляет на почту(TODO: надо бы разделить эти процессы)
        public static List<string> GetAccountInfo()
        {
            var result = new List<string>();
            var db = MySQL.DB_RS;
            {
                db.Open("SELECT * FROM accounts", MySQL.DB_CONN, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic);


                if (db.EOF)
                {
                    DeveloperConsole.Log("No users", ConsoleColor.Red);


                }

                for (var i = 1; !db.EOF; i++)
                {
                    result.Add(db.GetString(StringFormatEnum.adClipString, 1));
                }


                db.Close();
                return result;

            }
        } // Получает информацию о всех аккаунтах из БД
        public static string GetPlayerInfo(string username)
        {
            var db = MySQL.DB_RS;
            {
                db.Open("SELECT * FROM accounts WHERE Username='" + username + "'", MySQL.DB_CONN, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic);


                var result = "No such user";
                if (db.EOF)
                {
                    DeveloperConsole.Log("No such user", ConsoleColor.Red);
                    db.Close();
                    return null;

                }
                else
                {
                    DeveloperConsole.Log("User found", ConsoleColor.Green);
                    result = db.GetString();


                }



                db.Close();
                return result;

            }
        } // Получает информацию о конкретном аккаунте из БД

    }
}
