﻿namespace CardServer.Console
{
    public abstract class ConsoleCommand
    {
        public abstract string Name { get; protected set; } // Имя команды
        public abstract string Command { get; protected set; } // Текст команды
        public abstract string Description { get; protected set; } // Описание команды
        public abstract string Help { get; protected set; } // Hint

        public void AddCommandToConsole()
        {

            DeveloperConsole.AddCommandsToConsole(Command, this);

        } // Добавляет себя в список команд
        public abstract void RunCommand(string[] args); // Метод который будет вызываться при использовании команды
    }
}