﻿using System;

namespace CardServer.Console.Commands
{
    public class CommandGetPlayerInfo : ConsoleCommand
    {
        public sealed override string Name { get; protected set; }
        public sealed override string Command { get; protected set; }
        public sealed override string Description { get; protected set; }
        public sealed override string Help { get; protected set; }

        public CommandGetPlayerInfo()
        {
            Name = "Get Player Info";
            Command = "GetPlayerInfo";
            Description = "Write information about account";
            Help = "Use to get info about account. Usage: GetPlayerInfo [username]";

            AddCommandToConsole();
        }

        public override void RunCommand(string[] args)
        {
            if (args.Length < 2)
                DeveloperConsole.Log(Help, ConsoleColor.DarkMagenta);
            else
            {
                var info = Database.Database.GetPlayerInfo(args[1]);
                if (info != null)
                    DeveloperConsole.Log(info, ConsoleColor.Cyan);
            }

        }

        public static CommandGetPlayerInfo CreateCommand()
        {
            return new CommandGetPlayerInfo();
        }
    }
}
