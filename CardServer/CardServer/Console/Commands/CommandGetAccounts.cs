﻿using System;
using System.IO;

namespace CardServer.Console.Commands
{
    public class CommandGetAccounts : ConsoleCommand
    {
        public sealed override string Name { get; protected set; }
        public sealed override string Command { get; protected set; }
        public sealed override string Description { get; protected set; }
        public sealed override string Help { get; protected set; }

        public CommandGetAccounts()
        {
            Name = "Get Accounts Info";
            Command = "GetAccounts";
            Description = "Write accounts info in specific file";
            Help = "Use to get accounts info";

            AddCommandToConsole();
        }

        public override void RunCommand(string[] args)
        {
            var accountsInfo = Database.Database.GetAccountInfo(); ;
            var path = "ACCOUNTS.txt";
            if (args.Length > 1)
                path = args[1];

            DeveloperConsole.Log($"{accountsInfo.Count} accounts were found. Write info to {path}", ConsoleColor.Cyan);
            using (var sw = File.CreateText(path))
            {
                foreach (var userInfo in accountsInfo)
                {
                    sw.WriteLine(userInfo);
                }
            }

        }

        public static CommandGetAccounts CreateCommand()
        {
            return new CommandGetAccounts();
        }
    }
}
