﻿using System;

namespace CardServer.Console.Commands
{
    public class CommandQuit : ConsoleCommand
    {
        public sealed override string Name { get; protected set; }
        public sealed override string Command { get; protected set; }
        public sealed override string Description { get; protected set; }
        public sealed override string Help { get; protected set; }

        public CommandQuit()
        {
            Name = "Quit";
            Command = "quit";
            Description = "Quits the application";
            Help = "Use this command to quit";

            AddCommandToConsole();
        }

        public override void RunCommand(string[] args)
        {
            Environment.Exit(0);
        }

        public static CommandQuit CreateCommand()
        {
            return new CommandQuit();
        }
    }
}
