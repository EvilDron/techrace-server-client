﻿using CardServer.Net.Server;

namespace CardServer.Console.Commands
{
    public class CommandOnlineCheck : ConsoleCommand
    {
        public sealed override string Name { get; protected set; }
        public sealed override string Command { get; protected set; }
        public sealed override string Description { get; protected set; }
        public sealed override string Help { get; protected set; }

        public CommandOnlineCheck()
        {
            Name = "Check Online";
            Command = "online";
            Description = "Write count of connections";
            Help = "Use check online";

            AddCommandToConsole();
        }

        public override void RunCommand(string[] args)
        {
            DeveloperConsole.Log("Online is : " + ServerTCP.OnlineCount.ToString() + " players");
        }

        public static CommandOnlineCheck CreateCommand()
        {
            return new CommandOnlineCheck();
        }
    }
}
