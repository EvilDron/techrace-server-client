﻿using System;

namespace CardServer.Console.Commands
{
    public class CommandDeleteAccount : ConsoleCommand
    {
        public sealed override string Name { get; protected set; }
        public sealed override string Command { get; protected set; }
        public sealed override string Description { get; protected set; }
        public sealed override string Help { get; protected set; }

        public CommandDeleteAccount()
        {
            Name = "Delete Account";
            Command = "DeleteAccount";
            Description = "Delete account with such username";
            Help = "Use to delete account. Usage: DeleteAccount [username]";

            AddCommandToConsole();
        }

        public override void RunCommand(string[] args)
        {
            if (args.Length < 2)
                DeveloperConsole.Log(Help, ConsoleColor.DarkMagenta);
            else
                Database.Database.DeleteAccount(args[1]);
        }

        public static CommandDeleteAccount CreateCommand()
        {
            return new CommandDeleteAccount();
        }
    }
}
