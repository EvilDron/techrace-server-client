﻿using System;
using CardServer.Net.Server;

namespace CardServer.Console.Commands
{
    public class CommandSendMessage : ConsoleCommand
    {
        // Переопределяем свойства
        public sealed override string Name { get; protected set; } 
        public sealed override string Command { get; protected set; }
        public sealed override string Description { get; protected set; }
        public sealed override string Help { get; protected set; }

        public CommandSendMessage()
        {
            Name = "Send Message";
            Command = "SendMessage";
            Description = "Send message to client";
            Help = "Use to send message to client. Usage: SendMessage [userid] [message]";

            AddCommandToConsole();
        }

        public override void RunCommand(string[] args)
        {
            try
            {
                var message = "";
                for (var i = 2; i < args.Length; i++)
                {
                    message += args[i] + " ";
                }

                if (message.Length == 0)  throw new Exception();
                try
                {
                    ServerTCP.SendAlertTo(int.Parse(args[1]), message);
                    DeveloperConsole.Log("Successful", ConsoleColor.Green);
                }
                catch
                {
                    DeveloperConsole.Log("No such user", ConsoleColor.Red);
                }
            }
            catch
            {
                DeveloperConsole.Log(Help, ConsoleColor.DarkMagenta);
            }

        }

        public static CommandSendMessage CreateCommand()
        {
            return new CommandSendMessage();
        }
    }
}
