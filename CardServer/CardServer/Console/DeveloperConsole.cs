﻿using System;
using System.Collections.Generic;
using CardServer.Console.Commands;

namespace CardServer.Console
{
    public class DeveloperConsole
    {
        public static Dictionary<string, ConsoleCommand> Commands { get; private set; } // Слоаврь команд

        public static void Log(string log)
        {
            System.Console.WriteLine("[" + DateTime.Now.ToString("HH:mm:ss") + "] " + log);
        } // Выводит сообщение в консоль
        public static void Log(string log, ConsoleColor color)
        {
            System.Console.ForegroundColor = color;
            Log(log);
            System.Console.ForegroundColor = ConsoleColor.White;
        }// Перегрузка с цветом сообщения

        public static void InitConsole()
        {
            Commands = new Dictionary<string, ConsoleCommand>();
            CreateCommands();
        }// Инициализируем словарь команд
        private static void CreateCommands()
        {
            CommandQuit.CreateCommand();
            CommandOnlineCheck.CreateCommand();
            CommandGetAccounts.CreateCommand();
            CommandDeleteAccount.CreateCommand();
            CommandGetPlayerInfo.CreateCommand();
            CommandSendMessage.CreateCommand();
        } // Создаём экземпляры всех команд и добавляем их в список(фактически в статичном методе CreateCommand они сами создают экземпляр себя и в конструкторе добавляют себя в список)

        public static void AddCommandsToConsole(string name, ConsoleCommand command)
        {
            if (!Commands.ContainsKey(name))
            {
                Commands.Add(name, command);
            }
        }// Метод, вызываемый из конструкторов команд. Добавляет экзмемпляр команды в список



        public static void ParseInput(string command)
        {

            var input = command.Split(' ');// Разделяем ввод по пробелам,чтобы отделить команду и аргументы 

            if (!Commands.ContainsKey(input[0]))// Если команды нет в списке, говорим об этом
            {
                Log("Command not recognized.");
            }
            else
            {
                Commands[input[0]].RunCommand(input); // Вызываем метод реализущий функционал команды 
            }
        }// Разбирает введённую команду
    }
}