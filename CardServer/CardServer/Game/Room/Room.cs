﻿using CardServer.Console;
using CardServer.Net.Server;

namespace CardServer.Game.Room
{
    internal class Room
    {

        private long _first = -1; // Индекс первого игрока
        private long _second = -1; // Индекс второго игрока

        private readonly bool[,] _field = new bool[4, 4]; // Поле для кубиков(true - есть кубик, false - нет кубика)

        public long First
        {
            get => _first;
            set
            {
                if (_first == -1)
                    _first = value;
            }
        }
        public long Second
        {
            get => _second;
            set
            {
                if (_second == -1)
                    _second = value;
            }
        }

        public bool IsFull => _second > -1; // Есть ли в комнате 2 игрока
        public int CurrentRoomRating; // Текущий рейтинг комнаты, среднее арифметическое рейтингов игроков(Подразумевалось в дальнейшем увеличивать и уменьшать рейнг на это значение) 

        public void StartGame()
        {
            ServerTCP.SendGameStartTo(_first, _second); // Отправляем первому игроку сообщение о том, что игра началась
            ServerTCP.SendGameStartTo(_second, _first);// Отправляем второму игроку сообщение о том, что игра началась
        } // Запускаем игру в комнате

        public void SpawnCube(long index, int x, int y)
        {
            if (_second == index)
            {
                var t = x;
                x = 3 - x;
                y = 3 - y;
            }

            if (_field[x, y]) return;
            _field[x, y] = true;
            ServerTCP.SendSpawnCube(_first, x, y); // Отправляем первому клиенту команду заспавнить куб по заданным координатам
            ServerTCP.SendSpawnCube(_second, 3 - x, 3 - y); // У второго эти координаты зеркальны

        } // Пытаемся создать кубик по заданным координатам

        public void DeleteCube(long index, int x, int y)
        {
            var t = x;
            if (_second == index)
            {
                x = 3 - x;
                y = 3 - y;
            }

            if (!_field[x, y]) return;
            _field[x, y] = false;
            ServerTCP.SendDeleteCube(_first, x, y);
            ServerTCP.SendDeleteCube(_second, 3 - x, 3 - y);
        }// Пытаемся удалить кубик по заданным координатам

        public void Free()
        {
            DeveloperConsole.Log($"Room {PlayerManager.GetRoomIndex(_first)} has been closed");
            PlayerManager.RoomOut(_first);
            PlayerManager.RoomOut(_second);
        } // Освобождение комнаты

    }
}
