﻿using System;
using CardServer.Console;
using CardServer.Net.Server;

namespace CardServer.Game.Room
{
    internal class RoomManager
    {
        public static Room[] Rooms = new Room[Constants.MAX_PLAYER / 2]; // Создаём MAX_PLAYER / 2 комнат, так как больше не имеет смысла

        public static void PutInSearch(long index)
        {
            var rating = PlayerManager.GetRating(index); // Полчаем рейтинг игрока
            for (var i = 0; i < Constants.MAX_PLAYER / 2; i++)
            {
                if (Rooms[i] == null) // Если комната пустая, кладём туда игрока
                {
                    Rooms[i] = new Room {First = index, CurrentRoomRating = rating}; //  Создаём экземпляр комнаты с заданными параметрами
                    ServerTCP.Clients[index].RoomIndex = i; //  Говорим что игрок теперь в комнате с индексом i
                    return;
                }

                if (Math.Abs(Rooms[i].CurrentRoomRating - rating) >= Constants.MAX_RATING_DIFF ||
                    Rooms[i].IsFull) continue; // Если у комнаты неподходищий рейтинг или она заполенена продолжаем поиск

                ServerTCP.Clients[index].RoomIndex = i; //  Говорим что игрок теперь в комнате с индексом i
                Rooms[i].Second = index; // Говорим комнате что теперь у неё второй игрок имеет индекс index
                Rooms[i].CurrentRoomRating = (Rooms[i].CurrentRoomRating + rating) / 2; // Считаем средний рейтинг комнаты

                Rooms[i].StartGame(); // Запускаем игру в комнате
                DeveloperConsole.Log(
                    $"Room {i} is ready for game. First player is {ServerTCP.Clients[Rooms[i].First].GetName}. Second player is {ServerTCP.Clients[Rooms[i].Second].GetName}");
            }
        } // Кладёт игрока с заданным индексом в поиск(Определяем для него комнату: пробегаемся по комнатам и ищем ему или пустую или комнату с подхожящим ему рейтингом)TODO:Надо переписать, ибо доходит только до первой подходящей комнаты

        public static void TrySpawnCube(long index, int x, int y)=>
            Rooms[PlayerManager.GetRoomIndex(index)].SpawnCube(index, x, y); // Спавним в комнате куб

        public static void TryDeleteCube(long index, int x, int y) =>
            Rooms[PlayerManager.GetRoomIndex(index)].DeleteCube(index, x, y); // Удаляем в комнате куб

        public static void FreeRoom(long index)
        {
            Rooms[PlayerManager.GetRoomIndex(index)].Free();
            Rooms[PlayerManager.GetRoomIndex(index)] = null;

        } // Освобождаем комнату с заданным индексом

    }
}
