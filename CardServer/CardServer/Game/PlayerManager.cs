﻿using CardServer.Net.Server;

namespace CardServer.Game
{
    public class PlayerManager
    {
        public static string GetName(long index) => ServerTCP.Clients[index].GetName; // Возвращает имя пользвотеля с указанным индексом 
        public static int GetRating(long index) => ServerTCP.Clients[index].GetRating; // Возвращает рейтинг пользвотеля с указанным индексом 
        public static int GetRoomIndex(long index) => ServerTCP.Clients[index].RoomIndex; // Возвращает индекс комнаты пользвотеля с указанным индексом 
        public static void RoomOut(long index) => ServerTCP.Clients[index].RoomFree(); // Выбрасывает из комнаты  пользвотеля с указанным индексом
        public static bool IsInRoom(long index) => ServerTCP.Clients[index].RoomIndex !=-1; // В коинате ли в данный момент игрок
    }
}