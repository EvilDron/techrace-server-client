﻿using UnityEngine;
using System.Collections.Generic;
public class ClientHandleData : MonoBehaviour
{
    private delegate void Packet(byte[] data); // // Сигнатура всех методов обрабатывающих пакеты
    private static readonly Dictionary<long, Packet> Packets = new Dictionary<long, Packet>();//Словарь делегатов, ключ - тип пакета, значение - делегат который будет его обрабатывать
    private static long _pLength;
    private void Awake()
    {
        InitMessages();
    }

    private static void InitMessages()
    {

        Packets.Add((long)ServerPackets.SAlertMsg, Packet_SAlertMsg);

    }
    #region Обработка полученных пакетов (Можно не вникать, просто много проверок на кооректность формата пакета, и если всё номрально вызов соответсвующего ему делегата)
    public static void HandleData(byte[] data)
    {
        var playerBuffer = new ByteBuffer();
        var buffer = (byte[])data.Clone();

        playerBuffer.WriteBytes(buffer);

        if (playerBuffer.Count() == 0)
        {
            playerBuffer.Clear();
            return;
        }

        if (playerBuffer.Length() >= 8)
        {
            _pLength = playerBuffer.ReadLong(false);
            if (_pLength <= 0)
            {
                playerBuffer.Clear();
                return;
            }
        }
        if (playerBuffer.Length() >= 8)
        {
            _pLength = playerBuffer.ReadLong(false);
            if (_pLength <= 0)
            {
                playerBuffer.Clear();
                return;
            }
        }
        while (_pLength > 0 || _pLength <= playerBuffer.Length() - 8)
        {
            if (_pLength <= playerBuffer.Length() - 8)
            {
                playerBuffer.ReadLong();
                data = playerBuffer.ReadBytes((int)_pLength);
                HandleDataPackets(data);
            }
            _pLength = 0;
            if (playerBuffer.Length() < 8) continue;
            _pLength = playerBuffer.ReadLong(false);
            if (_pLength >= 0) continue;
            playerBuffer.Clear();
            return;
        }

    }
    public static void HandleDataPackets(byte[] data)
    {
        Packet packet;

        var buffer = new ByteBuffer();
        buffer.WriteBytes(data);
        var packetNumber = buffer.ReadLong();
        buffer = null;

        if (packetNumber == 0) return;

        if (Packets.TryGetValue(packetNumber, out packet))
        {
            packet.Invoke(data);
        }
    }
    #endregion

    #region PocketProcessing 

    private static void Packet_SAlertMsg(byte[] data)
    {
        var buffer = new ByteBuffer();

        buffer.WriteBytes(data);
        buffer.ReadLong(); // Вытаскиваем из буфера, ненужные после определения пакета, 8 байт

        var alertMessage = buffer.ReadString();

        Debug.Log("[Server] " + alertMessage);

        ClientSendData.SendReceiveOK(alertMessage);
    }
    #endregion

}
