﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkManager : MonoBehaviour
{

    public static ClientTCP ClientTcp = new ClientTCP();

    public int MyIndex;

    private void Awake()
    {
        DontDestroyOnLoad(this);
        UnityThread.initUnityThread();
    }

    private void Start()
    {
        Debug.Log("[NETWORK] Инициализация соединения...");
        ClientTcp.Connect();
    }

}
