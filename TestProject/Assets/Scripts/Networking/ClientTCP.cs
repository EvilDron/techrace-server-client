﻿using System;
using System.Net.Sockets;
using UnityEngine;

public class ClientTCP {

    public TcpClient PlayerSocket; // Наше подключение
    public NetworkStream MyStream; // Поток общения с сервером
    public bool Connecting; // Подключается ли игрок
    public bool Connected;// Подключаен ли игрок

    private byte[] _asyncBuff; // Асинхронный буфер записи и чтения
    public void Connect()
    {
        PlayerSocket = new TcpClient {ReceiveBufferSize = 4096, SendBufferSize = 4096, NoDelay = false}; // Создаём подключение с указанными параметрами
        _asyncBuff = new byte[8192];
        PlayerSocket.BeginConnect("127.0.0.1",5555, new AsyncCallback(ConnectCallback),PlayerSocket); // Пытаемся подключиться
        Connecting = true; 

    }

    private void ConnectCallback(IAsyncResult ar)
    {
        try
        {
            PlayerSocket.EndConnect(ar); // Заканчиваем процесс подключения
            if(PlayerSocket.Connected == false) // Если сервер не подключил нас
            {
                Connected = false;
                Connecting = false;
                return;
            }
            else
            {
                PlayerSocket.NoDelay = true;
                MyStream = PlayerSocket.GetStream(); // Получаем поток из подключения
                MyStream.BeginRead(_asyncBuff, 0, 8192, OnReceive, null); // Запускаем процесс ожидания пакетов от сервера
                Connected = true;
                Connecting = false;
                Debug.Log("[NETWORK] Подключение к серверу прошло успешно!");
            }
        }
        catch
        {
            Connected = false;
            Connecting = false;
            Debug.Log("[NETWORK] Невозможно подключиться к серверу.");

        }
    }// Ответ от сервера

    private void OnReceive(IAsyncResult ar)
    {
        try
        {
            var byteAmt = MyStream.EndRead(ar);// Кол-во полученных данных 
            if (byteAmt == 0) return; // Если пришел пустой пакет ничего не делаем
            var myBytes = new byte[byteAmt]; 
            Buffer.BlockCopy(_asyncBuff, 0, myBytes, 0, byteAmt); // Берём нужную информацию из полученных данных
            

            UnityThread.executeInUpdate(() =>
            {
                ClientHandleData.HandleData(myBytes);
            });// Запускаем асинхронную обработку запроса
            MyStream.BeginRead(_asyncBuff, 0, PlayerSocket.SendBufferSize, OnReceive, null); // Даём возомжность получать пакеты далее
        }
        catch (Exception ex)
        {
            Debug.LogError(ex);
        }
    }// Обрабатываем получение пакета

}
