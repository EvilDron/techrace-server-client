﻿public enum ServerPackets
{
    SWelcome = 1,
    SAlertMsg,
    SMatchStart,
}
public enum ClientPackets
{
    CRegisterAccount = 1,
    CLoginAccount,
    CSearchForRoom,
    CRecieveOK,
}