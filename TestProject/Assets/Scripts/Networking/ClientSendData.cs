﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ClientSendData : MonoBehaviour
{
    private static ClientTCP _clientTcp; // Наше подключение
    private void Start()
    {
        _clientTcp = NetworkManager.ClientTcp;
    }

    public static void SendDataToServer(byte[] data)
    {
        var buffer = new ByteBuffer();
        buffer.WriteBytes(data);
        _clientTcp.MyStream.Write(buffer.ToArray(), 0, buffer.ToArray().Length);
        buffer = null;
    }// Отправка сообщений на сервер

    #region PacketsToClient

    public static void SendReceiveOK(string msg)
    {

        var buffer = new ByteBuffer();
        buffer.WriteLong((long)ClientPackets.CRecieveOK);
        buffer.WriteString("[Client] " + msg);
        SendDataToServer(buffer.ToArray());
        buffer = null;
    }

    #endregion


}
